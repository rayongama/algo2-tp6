#ifndef MSETMIN_H
#define MSETMIN_H

#include <stdbool.h>
#include <stdlib.h>

typedef struct mset mset;

extern mset *mset_empty(int (*compar)(const void *, const void *));
extern const void *mset_add(mset *ms, const void *x);
extern const void *mset_remove(mset *ms, const void *x);
extern bool mset_is_empty(const mset *ms);
extern size_t mset_count(const mset *ms, const void *x);
extern size_t mset_card(const mset *ms);
extern const void *mset_min(const mset *ms);
extern void mset_dispose(mset **ptrms);
extern mset *mset_duplicate(const mset *ms);
extern const void *mset_remove_all(mset *ms, const void *x);

#endif  // MSETMIN_H
