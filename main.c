/*
 *  Affiche sur la sortie :
 *  - le nombre de mots lus sur l'entrée ;
 *  - la liste de ces mots, dans l'ordre lexicographique, chaque mot étant
 *      précédé de son nombre d'occurrences.
 *  Limitation : la longueur des mots est limitée à STRINGLEN_MAX caractères
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "msetmin.h"

#define STR(x)  #x
#define XSTR(x) STR(x)

#define STRINGLEN_MAX 31

#define CHECK_NOT_NULL(ptr, cause)                      \
    if ((ptr) == NULL) {                                \
      fprintf(stderr, "*** Fatal error: " cause "\n");  \
      exit(EXIT_FAILURE);                               \
    }

int main(void) {
  mset *ms = mset_empty((int (*)(const void *, const void *)) strcmp);
  CHECK_NOT_NULL(ms, "mset");
  char string[STRINGLEN_MAX + 1];
  while (scanf("%" XSTR(STRINGLEN_MAX) "s", string) == 1) {
    if (strlen(string) == STRINGLEN_MAX) {
      fprintf(stderr, "*** Warning: string '%s' possibly cut\n", string);
    }
    if (mset_count(ms, string) > 0) {
      CHECK_NOT_NULL(mset_add(ms, string), "mset");
    } else {
      char *t = malloc(strlen(string) + 1);
      CHECK_NOT_NULL(t, "string");
      strcpy(t, string);
      CHECK_NOT_NULL(mset_add(ms, t), "mset");
    }
  }
  printf("%zu\n", mset_card(ms));
  while (! mset_is_empty(ms)) {
    char *t = (char *) mset_min(ms);
    printf("%zu\t", mset_count(ms, t));
    printf("%s\n", t);
    /*
    while (mset_count(ms, t) > 0) {
      mset_remove(ms, t);
    }
    * */
    mset_remove_all(ms, t);
    free(t);
  }
  mset_dispose(&ms);
  return EXIT_SUCCESS;
}
