#include "msetmin.h"

typedef struct cmset cmset;

struct cmset {
  const void *value;
  size_t count;
  cmset *next;
};

struct mset {
  int (*compar)(const void *, const void *);
  cmset *head;
  size_t card;
};

mset *mset_empty(int (*compar)(const void *, const void *)){
  mset *ms = malloc(sizeof(*ms));
  if (ms == NULL){
    return NULL;
  }
  ms -> compar = compar;
  ms -> head = NULL;
  ms -> card = 0;
  return ms;
}
const void *mset_add(mset *ms, const void *x){
  cmset *cms = ms -> head;
  cmset *cms2 = NULL;
  while (cms != NULL){
    if (ms -> compar(cms -> value, x) == 0){
      cms -> count += 1;
      cms2 = cms;
      ms -> card += 1;
      break;
    }
    cms = cms -> next;
  }
  if (cms == NULL){
    cms2 = malloc(sizeof(*cms2));
    cms2 -> value = x;
    cms2 -> count = 1;
    cms2 -> next = ms -> head;
    ms -> head = cms2;
    ms -> card += 1;
  }
  return cms2;
}
const void *mset_remove(mset *ms, const void *x){
  cmset *cms = ms -> head;
  cmset *cms2 = NULL;
  while (cms != NULL){
    if (cms -> count > 0 && ms -> compar(cms -> value, x) == 0){
      cms -> count -= 1;
      ms -> card -= 1;
      cms2 = cms;
      break;
    }
    cms = cms -> next;
  }
  return cms2;
}
bool mset_is_empty(const mset *ms){
  return ms -> card == 0;
}
size_t mset_count(const mset *ms, const void *x){
  cmset *cms = ms -> head;
  while (cms != NULL){
    if (cms -> count > 0 && ms -> compar(cms -> value, x) == 0){
      return cms -> count;
      break;
    }
    cms = cms -> next;
  }
  return 0;
}
size_t mset_card(const mset *ms){
  return ms -> card;
}
const void *mset_min(const mset *ms){
  cmset *min = ms -> head;
  while (min != NULL && min -> count == 0){
    min = min -> next;
  }
  if (min == NULL){
    return NULL;
  }
  cmset *cms = min -> next;
  while (cms != NULL){
    if (cms -> count > 0 && ms -> compar(min -> value, cms -> value) > 0){
      min = cms;
    }
    cms = cms -> next;
  }
  return min -> value;
}
void mset_dispose(mset **ptrms){
  mset *ms = *ptrms;
  cmset *cms = ms -> head;
  while (cms != NULL){
    cmset *next = cms -> next;
    free(cms);
    cms = next;
  }
  free(ms);
}
mset *mset_duplicate(const mset *ms){
  return mset_empty(ms -> compar);
}
const void *mset_remove_all(mset *ms, const void *x){
  cmset *cms = ms -> head;
  cmset *cms2 = NULL;
  while (cms != NULL){
    if (cms -> count > 0 && ms -> compar(cms -> value, x) == 0){
      ms -> card -= cms -> count;
      cms -> count = 0;
      cms2 = cms;
      break;
    }
    cms = cms -> next;
  }
  return cms2;
}
